
/**
 * @file
 * Attach translate links to textareas
 */
 
/*jslint browser: true */
/*extern Drupal, $, jQuery, google */
/* last jslint: 2009-01-13 */

Drupal.googleLanguageTextarea = {

  /**
   * Preprocess the text before sending for translation.
   * Replace new lines with break tags, since Google doesn't return proper 
   * whitespace
   */
  preprocess: function(text) {
    text = text.replace(/\n/g, "<br>");
    return text;
  },

  /**
   * Post-process the text before sending for translation.
   */
  postprocess: function(text) {
    text = text.replace(/<br>/g, "\n");
    return text;
  },
  
  /**
   * Create an overlay over the specified element
   */
  overlayCreate: function(element) {
    var html = '<div class="translate-overlay"><div class="translate-overlay-title">';
    html += Drupal.t('Translating...');
    html += '</div><div class="translate-overlay-message"></div></div>'; 
    var overlay = $(html)
      .css({
        width:                $(element).outerWidth(),
        height:               $(element).outerHeight(),
        left:                 $(element).offset().left,
        top:                  $(element).offset().top
      })
      .appendTo($('body'));
    return overlay;
  },
  
  /**
   * Update the given overlay with an error message
   */
  overlayError: function(overlay, title, message) {
    overlay
      .find('.translate-overlay-title').html(title).end()
      .find('.translate-overlay-message').html(message + '<br/>' + Drupal.t('Click to continue')).end()
      .addClass('translate-error')
      .click(function() { $(this).remove(); });
  },
  
  /**
   * onClick call back for the translate link.
   */
  onClick: function() {
    var textarea_id = $(this).parents().filter('label').attr('for');
    var $textarea = $('#' + textarea_id);
  
    var overlay = Drupal.googleLanguageTextarea.overlayCreate($textarea);
    $textarea
      .val(Drupal.googleLanguageTextarea.preprocess($textarea.val()))
      .translate(Drupal.settings.googleLanguage.from, Drupal.settings.googleLanguage.to, {
        limit: 1000,
        data: false,
        walk: false,
        complete: function() {
          $textarea.val(Drupal.googleLanguageTextarea.postprocess($textarea.val()));
          overlay.remove();
        },
        error: function(error) {
          Drupal.googleLanguageTextarea.overlayError(overlay, Drupal.t('Translation Error'), error.message);
        },
        onTimeout: function() {
          this.stop();
          Drupal.googleLanguageTextarea.overlayError(overlay, Drupal.t('Translation Error'), Drupal.t('The Google translation service did not respond.'));
        },
        timeout: 30000 // is this milliseconds? I think so.
      });
    return false;
  }
};

/**
 * Drupal behavior to attach translate links to textareas
 */
Drupal.behaviors.googleLanguageTextarea = function(context) {
  $.translate().ready(function() {
    $('textarea', context).each(
      function() {
        var textarea = this;
        var link = $('<a></a>')
          .html(Drupal.t('Translate this text'))
          .addClass('google-translate')
          .attr('href', '#')
          .click(Drupal.googleLanguageTextarea.onClick);
        var branding = google.language.getBranding();
        $(branding).prepend('&nbsp;').prepend(link);
        $(textarea).parents().filter('.form-item').find('label').append(branding);
      }
    );
  });
};
